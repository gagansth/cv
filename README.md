# Gagan Shrestha
Dhobichaur, Kathmandu 

Mobile: 9843700477

gagansth9843@gmail.com

---

## Education
### Bachelors in Information Management(7th sem)
St. Xavier's College, Maitighar
### +2 Management(2013-2015)
People's Campus, Paknajol


## Skillset
 * HTML
 * PHP
 * C
 * Java
 * MySQL
 * SQLite
 * Wordpress
 * E-commerce 
 * Git

## Personal Profile

DOB: 23rd Nov, 1997

Gender: Male

Nationality: Nepali

Languages: English, Nepali, Hindi
